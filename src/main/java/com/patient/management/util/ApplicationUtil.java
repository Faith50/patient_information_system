package com.patient.management.util;

import java.security.SecureRandom;
import java.util.Random;

public class ApplicationUtil {

    public static final String API_VERSION = "/api/v1/";
    private static final Random RANDOM = new SecureRandom();
    private static final String ALPHABET = "1234567890987654321";
    private static final int PATIENT_ID_LIMIT = 10;


    public static String generateUniquePatientId() {
        StringBuilder main = new StringBuilder(PATIENT_ID_LIMIT);
        for (int i = 0; i < PATIENT_ID_LIMIT; i++) {
            main.append(ALPHABET.charAt(RANDOM.nextInt(ALPHABET.length())));
        }
        return main.toString();
    }
}
