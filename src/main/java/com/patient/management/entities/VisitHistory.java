/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.patient.management.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 *
 * @author user
 */

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "visithistory")

public class VisitHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "visitHistoryId")
    private int visitHistoryId;
    private String allergy;
    private String health_condition;
    private String medication;
    private Date dateVisited;
    private String familyHistory;

}
