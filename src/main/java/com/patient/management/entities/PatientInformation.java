/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.patient.management.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.util.Date;

/**
 *
 * @author user
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "patientinformation")

public class PatientInformation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "patientInformationId")
    private int patientInformationId;
    //@Pattern(regexp = "(\\+)?[0-9]{10}$", message = "generate patient id")
    @Column(name = "patientId")
    private long patientId;
    @Column(name = "firstName")
    private String firstName;
    @Column(name = "lastName")
    private String lastName;
    @Column(name = "address")
    private String address;
    @Column(name = "dateOfBirth")
    private Date dateOfBirth;
    @Column(name = "phoneNumber")
    private String phoneNumber;
    @Column(name = "email")
    private String email;
    @Column(name = "gender")
    private String gender;
    @Column(name = "occupation")
    private String occupation;
    @Column(name = "maritalStatus")
    private String maritalStatus;
    @Column(name = "age")
    private String age;
    @Column(name = "registeredDate")
    private Date registeredDate;
}
