/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.patient.management.repository;

import com.patient.management.entities.PatientInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 *
 * @author user
 */
@Repository
public interface PatientInformationRepository extends JpaRepository <PatientInformation, Integer>{
    List<PatientInformation> findByPatientInformationId(int patientInformationId);
    List<PatientInformation> findByPatientId(long patientId);
    List<PatientInformation> findByAge(String age);

}
