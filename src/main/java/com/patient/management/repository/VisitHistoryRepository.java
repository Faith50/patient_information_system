/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.patient.management.repository;


import com.patient.management.entities.VisitHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 *
 * @author user
 */
public interface VisitHistoryRepository extends JpaRepository<VisitHistory, Integer> {
    List<VisitHistory> findByVisitHistoryId(int visitHistoryId);

}
