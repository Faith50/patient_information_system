/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.patient.management.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.patient.management.entities.PatientInformation;
import com.patient.management.entities.VisitHistory;
import com.patient.management.exception.ResourceNotFoundException;
import com.patient.management.repository.PatientInformationRepository;
import com.patient.management.repository.VisitHistoryRepository;
import com.patient.management.services.PatientInformationServices;
import com.patient.management.services.VisitHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.ResourceAccessException;

import java.util.List;

/**
 *
 * @author user
 */

@CrossOrigin
@RestController
@RequestMapping("/api/v1")
@Service

public class patientInformationResource {
@Autowired private PatientInformationServices patientInformationServices;
@Autowired private PatientInformationRepository patientInformationRepository;
@Autowired private VisitHistoryRepository visitHistoryRepository;
@Autowired private VisitHistoryService visitHistoryService;

    @PostMapping("/addpatient")
    public ResponseEntity CreatePatient(@RequestBody String response) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        PatientInformation patientInformation = objectMapper.readValue(response, new TypeReference<PatientInformation>() {
        });
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return (patientInformationServices.createPatientInformation(response)) ? new ResponseEntity(patientInformation, HttpStatus.OK) : new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/listpatient")
    public ResponseEntity ListPatient() throws Exception {
        List<PatientInformation> patientInformationsList = patientInformationServices.findAll();
        return (patientInformationsList == null) ? new ResponseEntity(patientInformationsList,HttpStatus.NOT_FOUND) : new ResponseEntity(patientInformationsList, HttpStatus.OK);
    }

//    @GetMapping("/listpatient/{id}")
//    public ResponseEntity ListPatientRecord(@RequestParam("patientId") long patientId) throws Exception {
//        List<PatientInformation> patientRecord = patientInformationServices.findByPatientId(patientId);
//
//        return (patientRecord == null) ? new ResponseEntity(patientRecord,HttpStatus.NOT_FOUND) : new ResponseEntity(patientRecord, HttpStatus.OK);
//    }

    @GetMapping("patient/{id}")
    public ResponseEntity<?> ListPatientInformationById(@PathVariable (value = "id") long patientId)
            throws ResourceNotFoundException{
        List<PatientInformation> patientInformation = patientInformationRepository.findByPatientId(patientId);
        return ResponseEntity.ok().body(patientInformation);
    }

//    @GetMapping("singlepatient/{id}")
//    public ResponseEntity<PatientInformation> ListPatientById(@PathVariable (value = "id") long patientId)
//            throws ResourceNotFoundException{
//        PatientInformation patientInfo = patientInformationRepository.findByPatientId(patientId).orElseThrow(
//                () -> new ResourceAccessException("patientInformation not :: " + patientId));
//        return ResponseEntity.ok().body(patientInfo);
//    }


    @PostMapping("/addvisit")
    public ResponseEntity addPatientVisit(@RequestBody String response) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        VisitHistory visitHistory = objectMapper.readValue(response, new TypeReference<VisitHistory>() {
        });
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return (visitHistoryService.createVisit(response)) ? new ResponseEntity(visitHistory, HttpStatus.OK) : new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/listvisits")
    public ResponseEntity ListVisitHistory() throws Exception {
        List<VisitHistory> visitHistoryList = visitHistoryService.findAll();
        return (visitHistoryList == null) ? new ResponseEntity(visitHistoryList,HttpStatus.NOT_FOUND) : new ResponseEntity(visitHistoryList, HttpStatus.OK);
    }

    @GetMapping("visithistory/{id}")
    public ResponseEntity<VisitHistory> ListVisitHistoryById(@PathVariable (value = "id") int visitHistoryId)
            throws ResourceNotFoundException{
        VisitHistory visitHistory = visitHistoryRepository.findById(visitHistoryId).orElseThrow(
                () -> new ResourceAccessException("Visit History not :: " + visitHistoryId));
        return ResponseEntity.ok().body(visitHistory);
    }
}
