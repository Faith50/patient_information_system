/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.patient.management.services;

import com.patient.management.entities.PatientInformation;

import java.util.List;

/**
 *
 * @author user
 */
public interface PatientInformationServices {
    public boolean createPatientInformation(final String response) throws Exception;
    public List<PatientInformation> findByPatientId(long patientId) throws Exception;
    public List<PatientInformation> getPatientByPatientInformationId(int patientInformationId) throws Exception;
    public PatientInformation findById(int id) throws Exception;
    public List<PatientInformation> findAll() throws Exception;

}
