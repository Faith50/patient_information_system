/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.patient.management.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.patient.management.entities.VisitHistory;
import com.patient.management.repository.VisitHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 *
 * @author user
 */
@Service
public class VisitHistoryServiceImplementation implements VisitHistoryService{
    @Autowired private VisitHistoryRepository visitHistoryRepository;

    public boolean createVisit(final String response) throws Exception {
        VisitHistory visitHistory = new VisitHistory();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        visitHistory = objectMapper.readValue(response, new TypeReference<VisitHistory>() {
        });
        return visitHistoryRepository.save(visitHistory) != null;
    }

    public List<VisitHistory> findAll() throws Exception {
        return visitHistoryRepository.findAll();
    }

    public VisitHistory findById(int id) throws Exception {

        Optional<VisitHistory> optionalVisitHistory = visitHistoryRepository.findById(id);
        return optionalVisitHistory.get();
    }
    public List<VisitHistory> getVisitHistoryById(int visitHistoryId) throws Exception {
        try {

            List<VisitHistory> listAll = visitHistoryRepository.findByVisitHistoryId(visitHistoryId);
            return visitHistoryRepository.findByVisitHistoryId(visitHistoryId);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<VisitHistory> findByVisitHistoryId(int visitHistoryId) throws Exception {
        return visitHistoryRepository.findByVisitHistoryId(visitHistoryId);
    }


}
