/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.patient.management.services;

import com.patient.management.entities.VisitHistory;

import java.util.List;

/**
 *
 * @author user
 */
public interface VisitHistoryService {
    List<VisitHistory> findByVisitHistoryId(int visitHistoryId) throws Exception;
    List<VisitHistory> getVisitHistoryById(int visitHistoryId) throws Exception;
    VisitHistory findById(int id) throws Exception;
    List<VisitHistory> findAll() throws Exception;
    boolean createVisit(final String response) throws Exception;
}
