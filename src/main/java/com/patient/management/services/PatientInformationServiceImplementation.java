/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.patient.management.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.patient.management.entities.PatientInformation;
import com.patient.management.repository.PatientInformationRepository;
import com.patient.management.util.ApplicationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 *
 * @author user
 */
@Service
public class PatientInformationServiceImplementation implements PatientInformationServices{

    @Autowired private PatientInformationRepository patientInformationRepository;


    public boolean createPatientInformation(final String response) throws Exception {
        PatientInformation patientInformation = new PatientInformation();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        patientInformation = objectMapper.readValue(response, new TypeReference<PatientInformation>() {

        });
        long randomPatientId = (long) Math.floor(Math.random() * 9_000_000_000L) + 1_000_000_000L;
        patientInformation.setPatientId(randomPatientId);

        return patientInformationRepository.save(patientInformation) != null;
    }

    public List<PatientInformation> findAll() throws Exception {
        return patientInformationRepository.findAll();
    }

    public PatientInformation findById(int id) throws Exception {

        Optional<PatientInformation> optionalPatientInformation = patientInformationRepository.findById(id);
        return optionalPatientInformation.get();
    }
    public List<PatientInformation> getPatientByPatientInformationId(int patientInformationId) throws Exception {
        try {

            List<PatientInformation> listAll = patientInformationRepository.findByPatientInformationId(patientInformationId);
            return patientInformationRepository.findByPatientInformationId(patientInformationId);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<PatientInformation> findByPatientId(long patientId) throws Exception {
        return patientInformationRepository.findByPatientId(patientId);
    }


}
