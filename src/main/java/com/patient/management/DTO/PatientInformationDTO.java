/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.patient.management.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 *
 * @author user
 */
@Data
@AllArgsConstructor
@NoArgsConstructor

public class PatientInformationDTO {
    private String firstName;
    private String lastName;
    private String address;
    private Date dateOfBirth;
    private String phoneNumber;
    private String email;
    private String gender;
    private String occupation;
    private String maritalStatus;
    private Date registeredDate;
}
